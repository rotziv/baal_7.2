/*Copyright (C) 
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * 2021 - Chavez Aquiagual Victor Manuel
 */

 // ("Basado en la DLL de francisco dot rodriguez at ingenieria dot unam dot edu")

#ifndef  DLL_INC
#define  DLL_INC
#define TAM 1024 // Buffer size

#include <stdlib.h>
// para malloc and friends y size_t

#include <stdbool.h>
// para bool and friends

#include <assert.h>
// para las aserciones: assert()

#include <stdio.h>
// para fprintf()

#include <string.h>
// para string

#include <unistd.h>
// para usar delay en mensajes

#include <time.h>
// para el tiempo de reproducción

#include <errno.h>
// para los informes de errores

#include <sys/select.h>
// para la estructura timeval

#include <ctype.h>
// para el UPPERCASE

typedef struct Album
{
	char* nombreArtista;
	char* nombreAlbum;
	char* comentario;
	size_t anhio;
	char* genero;
	size_t numCanciones;
	char** nombreCancion;
	size_t* calificacionCancion;
	size_t calificacionAlbum;
	size_t* duracion;
} Album;

typedef struct Node
{
	Album* unAlbum;

	struct Node* next;
	struct Node* prev;
} Node;

typedef struct
{
	Node* first;
	Node* last;
	Node* cursor;
	size_t len;
} DLL;

DLL* DLL_New( void );
void DLL_Delete( DLL** this );

Node* DLL_Front( DLL* this );
Node* DLL_Back( DLL* this );

void DLL_Insert( DLL* this, Node* item );
void DLL_Push_front( DLL* this, Node* item );
void DLL_Push_back( DLL* this, Node* item );

void DLL_Pop_front( DLL* this );
void DLL_Pop_back( DLL* this );
Node* DLL_Erase( DLL* this, Node* pos );

Node* DLL_Clone( DLL* this, Node* pos );
Node* DLL_Edit( DLL* this, Node* pos, int field );
void DLL_MakeEmpty( DLL* this );

void DLL_Cursor_front( DLL* this );
void DLL_Cursor_back( DLL* this );
void DLL_Cursor_next( DLL* this );
void DLL_Cursor_prev( DLL* this );
void DLL_Cursor_set( DLL* this, Node* pos );
Node* DLL_Cursor_Get( DLL* this );

bool DLL_IsEmpty( DLL* this );
size_t DLL_Len( DLL* this );

Node* DLL_Find_nombreArtista( DLL* this, const char* key );
Node* DLL_Find_nombreAlbum( DLL* this, const char* key );
Node* DLL_Find_anhio( DLL* this, size_t key );
Node* DLL_Find_genero( DLL* this, const char* key );
Node* DLL_Find_numCanciones( DLL* this, size_t key );
Node* DLL_Find_nombreCancion( DLL* this, const char* key, size_t* index );
Node* DLL_Find_calificacionCancion( DLL* this, size_t key, size_t* index );
Node* DLL_Find_calificacionAlbum( DLL* this, size_t key );
Node* DLL_Find_duracion( DLL* this, size_t key, size_t* index );

void DLL_PrintStructure( DLL* this );

void DLL_Leer( DLL* this );
void DLL_Duplicar( DLL* this );
void DLL_Modificar( DLL* this );
void DLL_Borrar( DLL* this, int mode );
void DLL_Guardar ( DLL* this );
void DLL_Cargar ( DLL* this );

void DLL_Imprimir( DLL* this, int field, int rating );
void DLL_Imprimir_Album( DLL* this, Node* item );
void DLL_Busqueda( DLL* this, int mode );

void DLL_Reproducir( DLL* this );

#endif   /* ----- #ifndef DLL_INC  ----- */
