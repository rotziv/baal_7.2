
#include <stdio.h>
#include "DLL.h"
#include "Entorno.h"

//----------------------------------------------------------------------
//  Driver program
//----------------------------------------------------------------------
int main( void )
{
   Entorno_Construir();

   DLL* biblioteca = DLL_New();
   DLL_Cargar( biblioteca );

   int opc = 0;

   //Entorno_Menu_Tutorial();

   do{
      opc = Entorno_Menu_Principal();

      if( opc == 1 ){
         do{
            opc = Entorno_Menu_1();

            if( opc < 5 ) DLL_Imprimir( biblioteca, opc, 0 );

            else if( opc == 5 ){
               do{
                  opc = Entorno_Menu_1_5y6( "álbum" );
                  if( opc != 8 ) DLL_Imprimir( biblioteca, 5, opc );
               } while( opc != 8 );

            } else if( opc == 6 ){
               do{
                  opc = Entorno_Menu_1_5y6( "pista" );
                  if( opc != 8 ) DLL_Imprimir( biblioteca, 6, opc );
               } while( opc != 8 );
            }
         } while( opc != 7 );

      } else if( opc == 2 ){
         do{
            opc = Entorno_Menu_2();
            if( opc != 5 ) DLL_Busqueda( biblioteca, opc );
         } while( opc != 5 );

      } else if( opc == 3 ){
         do{
            opc = Entorno_Menu_3();
            if( opc == 1 ) DLL_Leer( biblioteca );
            else if( opc == 2 ) DLL_Duplicar( biblioteca );
            else if( opc == 3 ) DLL_Modificar( biblioteca );
            else if( opc != 10 ) DLL_Borrar( biblioteca, opc );
         } while( opc != 10 );
         DLL_Guardar( biblioteca );
         
      } else if( opc == 4 ){
         do{
            opc = Entorno_Menu_4();
            Entorno_Construir();

            if( opc == 1 ){
               printf( "Cargar la biblioteca" ); Entorno_Gotoxy( 2, 4 );
               printf( "La biblioteca se ha cargado con éxito" );
               DLL_MakeEmpty( biblioteca );
               DLL_Cargar( biblioteca );
               Entorno_Mensaje_Continuar();
            } else if( opc == 2 ){
               printf( "Guardar la biblioteca" ); Entorno_Gotoxy( 2, 4 );
               printf( "La biblioteca se ha guardado con éxito" );
               DLL_Guardar( biblioteca );
               Entorno_Mensaje_Continuar();
            } else if( opc == 3 ){
               DLL_PrintStructure( biblioteca );
            }
            
         } while( opc != 4 );
      } else if( opc == 5 ){
         DLL_Reproducir( biblioteca );
      }

   }while( opc != 6 );

   DLL_Guardar( biblioteca );
   DLL_Delete( &biblioteca );

   return 1;
}
