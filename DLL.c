/*Copyright (C) 
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * 2021 - Chavez Aquiagual Victor Manuel
 */
 
 // ("Basado en la DLL de francisco dot rodriguez at ingenieria dot unam dot edu")

#include "DLL.h"
#include "Entorno.h"

static Node* new_node( Album* unAlbum )
{
   Node* n = (Node*) malloc( sizeof( Node ) );
   
   if( n != NULL ){
      n->unAlbum = unAlbum;

      n->next = NULL;
      n->prev = NULL;
   }

   return n;
}

static void free_node( Node** this )
{
   assert( *this );

   free( (*this)->unAlbum->nombreArtista );
   free( (*this)->unAlbum->nombreAlbum );
   free( (*this)->unAlbum->comentario );
   free( (*this)->unAlbum->genero );
   free( (*this)->unAlbum->calificacionCancion );
   free( (*this)->unAlbum->duracion );
   (*this)->unAlbum->nombreArtista = NULL;
   (*this)->unAlbum->nombreAlbum = NULL;
   (*this)->unAlbum->comentario = NULL;
   (*this)->unAlbum->genero = NULL;
   (*this)->unAlbum->calificacionCancion = NULL;
   (*this)->unAlbum->duracion = NULL;
   for ( size_t i = 0; i < (*this)->unAlbum->numCanciones; i++ ){
      free( (*this)->unAlbum->nombreCancion[i] );
      (*this)->unAlbum->nombreCancion[i] = NULL;
   }
   free( (*this)->unAlbum->nombreCancion );
   (*this)->unAlbum->nombreCancion = NULL;

   free( (*this)->unAlbum );
   (*this)->unAlbum = NULL;

   free( *this );
   *this = NULL;
}

static char* uppercase( const char* word )
{
   char* buffer = (char*) malloc( strlen( word ) * sizeof( char ) );

   for( size_t i = 0; i < strlen( word ); i++ )
      buffer[i] = toupper( word[0] );

   return buffer;

   free ( buffer );
   buffer = NULL;
}

static char* estrellas( size_t calificacion )
{
   char* buffer = (char*) malloc( TAM * sizeof( char ) );

   if( calificacion == 0 ){
      strcpy( buffer, "☆☆☆☆☆" );
   } else if( calificacion == 1 ){
      strcpy( buffer, "★☆☆☆☆" );
   } else if( calificacion == 2 ){
      strcpy( buffer, "★★☆☆☆" );
   } else if( calificacion == 3 ){
      strcpy( buffer, "★★★☆☆" );
   } else if( calificacion == 4 ){
      strcpy( buffer, "★★★★☆" );
   } else if( calificacion == 5 ){
      strcpy( buffer, "★★★★★" );
   } else{
      strcpy( buffer, "Not rated" );
   }

   return buffer;

   free ( buffer );
   buffer = NULL;
}

static void invertRating( int* rating )
{
   if( *rating == 1 ) *rating = 5;
   else if( *rating == 2 ) *rating = 4;
   else if( *rating == 4 ) *rating = 2;
   else if( *rating == 5 ) *rating = 1;
   else if( *rating == 6 ) *rating = 0;
}

static char* ssm_2_hms( size_t ssm )
{
   char* buffer = (char*) malloc( TAM * sizeof( char ) );

   size_t h = ( ssm / 3600 );
   size_t m = ( ssm - ( 3600 * (h) ) ) / 60;
   size_t s = ( ssm - ( 3600 * (h) ) - ( 60 * (m) ) );

   if ( h == 0 ){
      sprintf( buffer, "%02ld:%02ld", m, s );
   } else{
      sprintf( buffer, "%02ld:%02ld:%02ld", h, m, s );
   }

   return buffer;

   free ( buffer );
   buffer = NULL;
}

static int comparador( const void* a, const void* b )
{
   return strcmp( uppercase( *(const char**)a ), uppercase( *(const char**)b ) );
}

/**
 * @brief Crea una lista doblemente enlazada
 *
 * @return Una referencia a la nueva lista
 * @post Una lista existente en el heap
 */
DLL* DLL_New( void )
{
   DLL* list = (DLL*) malloc( sizeof( DLL ) );

   if( list != NULL ){
      list->first = NULL;
      list->last = NULL;
      list->cursor = NULL;
      list->len = 0;
   }

   return list;
}

/**
 * @brief Destruye una lista.
 *
 * @param this Una lista.
 */
void DLL_Delete( DLL** this )
{
   assert( *this );

   while( (*this)->first != NULL ){
      DLL_Pop_back( *this );
   }

   free( *this );
   *this = NULL;
}

/**
 * @brief Devuelve una copia del TAD en el frente de la lista.
 *
 * @param this Una lista.
 *
 * @return La copia del TAD en el frente de la lista.
 *
 * @post Si la lista está vacía se dispara una aserción.
 */
Node* DLL_Front( DLL* this )
{
   assert( this->first != NULL );

   return this->first;
}

/**
 * @brief Devuelve una copia del TAD en la parte trasera de la lista.
 *
 * @param this Una lista.
 *
 * @return La copia del TAD en la parte trasera de la lista.
 *
 * @post Si la lista está vacía se dispara una aserción.
 */
Node* DLL_Back( DLL* this )
{
   assert( this->first != NULL );

   return this->last;
}

/**
 * @brief Inserta un elemento a la derecha del cursor.
 *
 * @param lista Una referencia a la lista de trabajo
 * @param item El elemento a insertar
 */
void DLL_Insert( DLL* this, Node* item )
{
   assert( item );

   if( this->first != NULL ){
      if( this->cursor->next == NULL ){
         DLL_Push_back( this, item );
      } else{
         item->prev = this->cursor; 
         DLL_Cursor_next( this ); 
         item->next = this->cursor; 
         DLL_Cursor_prev( this ); 
         this->cursor->next = item; 
         DLL_Cursor_next( this ); 
         DLL_Cursor_next( this ); 
         this->cursor->prev = item; 
         DLL_Cursor_prev( this );
         ++this->len;
      }
   } else{  
      this->first = this->last = this->cursor = item;
      ++this->len;
   }
}

/**
 * @brief Inserta un elemento en el frente de la lista.
 *
 * @param this Una lista.
 * @param item El elemento a insertar
 */
void DLL_Push_front( DLL* this, Node* item )
{
   assert( item );

   if( this->first != NULL ){
      this->first->prev = item;
      item->next = this->first;
      this->first = item;
   } else{
      this->first = this->last = this->cursor = item;
   }
   ++this->len;
}

/**
 * @brief Inserta un elemento en el fondo de la lista.
 *
 * @param this Una lista.
 * @param item El elemento a insertar
 */
void DLL_Push_back( DLL* this, Node* item )
{
   assert( item );

   if( this->first != NULL ){
      this->last->next = item;
      item->prev = this->last;
      this->last = item;
   } else{
      this->first = this->last = this->cursor = item;
   }
   ++this->len;
}

/**
 * @brief Elimina el elemento al frente de la lista.
 *
 * @param this Referencia a una lista.
 *
 * @post El cursor se mantiene en la posición en la que estaba cuando entró la
 * función.
 */
void DLL_Pop_front( DLL* this )
{
   assert( this->len );

   if( this->last != this->first ){
      Node* x = this->first->next;
      x->prev = NULL;
      free_node( &(this->first) );
      this->first = x;
   } else{
      free_node( &(this->first) );
      this->first = this->last = this->cursor = NULL;
   }
   --this->len;
}

/**
 * @brief Elimina el elemento al fondo de la lista.
 *
 * @param this Referencia a una lista.
 *
 * @post El cursor se mantiene en la posición en la que estaba cuando entró la
 * función.
 */
void DLL_Pop_back( DLL* this )
{
   assert( this->len );

   if( this->last != this->first ){
      Node* x = this->last->prev;
      x->next = NULL;
      free_node( &(this->last) );
      this->last = x;
   } else{
      free_node( &(this->last) );
      this->first = this->last = this->cursor = NULL;
   }
   --this->len;
}

/**
 * @brief Elimina un elemento.
 *
 * @param this Una lista.
 * @param pos Una posición válida. Éste se puede obtener de las diferentes funciones de movimiento del cursor y de las funciones de búsqueda.
 *
 * @return Una referencia al elemento a la derecha del elemento que se acaba de borrar.
 */
Node* DLL_Erase( DLL* this, Node* pos )
{
   assert( this->len );

   if( this->first == pos ){
      DLL_Pop_front( this );
   } else if( this->last == pos ){
      DLL_Pop_back( this );
   } else{
      Node* x = pos->prev;
      x->next = pos->next;
      Node* y = pos->next;
      y->prev = pos->prev;
      free_node( &(pos) );
      --this->len;
   }

   return this->cursor;
}

/**
 * @brief Clona un elemento.
 *
 * @param this Una lista.
 * @param pos Una posición válida. Éste se puede obtener de las diferentes funciones de movimiento del cursor y de las funciones de búsqueda.
 *
 * @return Una referencia al elemento que se acaba de clonar.
 */
Node* DLL_Clone( DLL* this, Node* pos )
{
   assert( this->len );

   Album* unAlbum = (Album*) malloc( sizeof( Album ) );

   unAlbum->nombreArtista = (char*) malloc( strlen( pos->unAlbum->nombreArtista ) * sizeof( char ) );
   strcpy( unAlbum->nombreArtista, pos->unAlbum->nombreArtista );

   unAlbum->nombreAlbum = (char*) malloc( strlen( pos->unAlbum->nombreAlbum ) * sizeof( char ) );
   strcpy( unAlbum->nombreAlbum, pos->unAlbum->nombreAlbum );

   unAlbum->calificacionAlbum = pos->unAlbum->calificacionAlbum;

   unAlbum->comentario = (char*) malloc( strlen( pos->unAlbum->comentario ) * sizeof( char ) );
   strcpy( unAlbum->comentario, pos->unAlbum->comentario );

   unAlbum->anhio = pos->unAlbum->anhio;
   
   unAlbum->genero = (char*) malloc( strlen( pos->unAlbum->genero ) * sizeof( char ) );
   strcpy( unAlbum->genero, pos->unAlbum->genero );

   unAlbum->numCanciones = pos->unAlbum->numCanciones;
   
   unAlbum->nombreCancion = (char**) malloc( unAlbum->numCanciones * sizeof( char* ) );
   unAlbum->calificacionCancion = (size_t*) malloc( unAlbum->numCanciones * sizeof( size_t ) );
   unAlbum->duracion = (size_t*) malloc( unAlbum->numCanciones * sizeof( size_t ) );

	for ( size_t i = 0; i < unAlbum->numCanciones; i++ ){
      unAlbum->nombreCancion[i] = (char*) malloc( strlen( pos->unAlbum->nombreCancion[i] ) * sizeof( char ) );
      strcpy( unAlbum->nombreCancion[i], pos->unAlbum->nombreCancion[i] );

      unAlbum->calificacionCancion[i] = pos->unAlbum->calificacionCancion[i];

      unAlbum->duracion[i] = pos->unAlbum->duracion[i];
   }
   
   Node* item = new_node( unAlbum );
   DLL_Cursor_set( this, pos );
   DLL_Insert( this, item );

   return item;
}

/**
 * @brief Edita un elemento.
 *
 * @param this Una lista.
 * @param pos Una posición válida. Éste se puede obtener de las diferentes funciones de movimiento del cursor y de las funciones de búsqueda.
 *
 * @return Una referencia al elemento que se acaba de editar.
 */
Node* DLL_Edit( DLL* this, Node* pos, int field )
{
   assert( this->len );

   char* buffer = (char*) malloc( TAM * sizeof( char ) );
   int index = 0;

   if( field == 1 ){
      printf( "Ingrese el nuevo nombre del artista" ); Entorno_Gotoxy( 5, 41 );
	   fgets( buffer, TAM, stdin );
      strcpy( pos->unAlbum->nombreArtista, buffer );
      Entorno_Mensaje_Modificacion();
   } else if( field == 2 ){
      printf( "Ingrese el nuevo nombre del álbum" ); Entorno_Gotoxy( 5, 41 );
	   fgets( buffer, TAM, stdin );
      strcpy( pos->unAlbum->nombreAlbum, buffer );
      Entorno_Mensaje_Modificacion();
   } else if( field == 3 ){
      printf( "Ingrese el nuevo comentario del álbum" ); Entorno_Gotoxy( 5, 41 );
	   fgets( buffer, TAM, stdin );
      strcpy( pos->unAlbum->comentario, buffer );
      Entorno_Mensaje_Modificacion();
   } else if( field == 4 ){
      printf( "Ingrese el nuevo año del álbum" ); Entorno_Gotoxy( 5, 41 );
	   scanf( "%ld", &(pos->unAlbum->anhio) );
      fgets( buffer, TAM, stdin );
      Entorno_Mensaje_Modificacion();
   } else if( field == 5 ){
      printf( "Ingrese el nuevo género del álbum" ); Entorno_Gotoxy( 5, 41 );
	   fgets( buffer, TAM, stdin ); 
      strcpy( pos->unAlbum->genero, buffer );
      Entorno_Mensaje_Modificacion();
   } else if( field == 6 ){
      printf( "Ingrese el número de la canción que desea modificar" ); Entorno_Gotoxy( 5, 41 );
	   scanf( "%d", &index );
      fgets( buffer, TAM, stdin );

      if( index > pos->unAlbum->numCanciones || index < 1 ){
         Entorno_Puntero_Terminal();
         Entorno_Mensaje_Error();
      } else{
         DLL_Imprimir_Album( this, pos );
         printf( "Ingrese el nuevo nombre de la canción %d", index ); Entorno_Gotoxy( 5, 41 );
	      fgets( buffer, TAM, stdin );
         strcpy( pos->unAlbum->nombreCancion[index-1], buffer );
         Entorno_Mensaje_Modificacion();
      }

   } else if( field == 7 ){
      DLL_Imprimir_Album( this, pos );
      printf( "Ingrese número de la canción que desea modificar" ); Entorno_Gotoxy( 5, 41 );
	   scanf( "%ld", &(pos->unAlbum->anhio) );
      fgets( buffer, TAM, stdin );

      if( index > pos->unAlbum->numCanciones || index < 1 ){
         Entorno_Puntero_Terminal();
         Entorno_Mensaje_Error();
      } else{
         Entorno_Construir();
         DLL_Imprimir_Album( this, pos );
         printf( "Ingrese la nueva calificación de la canción %d", index ); Entorno_Gotoxy( 5, 41 );
	      scanf( "%ld", &(pos->unAlbum->calificacionCancion[index-1]) );
         fgets( buffer, TAM, stdin );
         Entorno_Mensaje_Modificacion();
      }
   } else{
      printf( "Ingrese la nueva calificación del álbum" ); Entorno_Gotoxy( 5, 41 );
	   scanf( "%ld", &(pos->unAlbum->calificacionAlbum) );
      fgets( buffer, TAM, stdin );
      Entorno_Mensaje_Modificacion();
   }

   free( buffer );
   buffer = NULL;

   return pos;
}

/**
 * @brief Elimina todos los elementos de la lista sin eliminar la lista.
 *
 * @param this Referencia a una lista.
 */
void DLL_MakeEmpty( DLL* this )
{
   assert( this );
   
   while( this->first != NULL )
      DLL_Pop_back( this );
}

/**
 * @brief Coloca al cursor al inicio de la lista.
 *
 * @param this Una referencia a la lista de trabajo
 */
void DLL_Cursor_front( DLL* this )
{
   this->cursor = this->first;
}

/**
 * @brief Coloca al cursor al final de la lista.
 *
 * @param this Una referencia a la lista de trabajo
 */
void DLL_Cursor_back( DLL* this )
{
   this->cursor = this->last;
}

/**
 * @brief Mueve al cursor al siguiente elemento a la derecha.
 *
 * @param this Una lista.
 */
void DLL_Cursor_next( DLL* this )
{
   assert( this->cursor != NULL );

   Node* right = this->cursor->next;
   this->cursor = right;
}

/**
 * @brief Mueve al cursor al siguiente elemento a la izquierda.
 *
 * @param this Una lista.
 */
void DLL_Cursor_prev( DLL* this )
{
   assert( this->cursor != NULL );

   Node* left = this->cursor->prev;
   this->cursor = left;
}

/**
 * @brief Pone el cursor en una posición.
 *
 * @param this Una lista.
 * @param pos Una posición válida. Éste se puede obtener de las diferentes funciones de movimiento del cursor y de las funciones de búsqueda.
 */
void DLL_Cursor_set( DLL* this, Node* pos )
{
   assert( DLL_IsEmpty( this ) == false );

   this->cursor = pos;
}

/**
 * @brief Devuelve una copia del TAD en el cursor de la lista.
 *
 * @param this Una lista.
 *
 * @return La copia del TAD en el cursor de la lista.
 *
 * @post Si la lista está vacía se dispara una aserción.
 */
Node* DLL_Cursor_Get( DLL* this )
{
   assert( this->cursor );

   return this->cursor;
}

/**
 * @brief Indica si la lista está vacía.
 *
 * @param this Referencia a una lista.
 *
 * @return true si la lista está vacía; false en caso contrario.
 */
bool DLL_IsEmpty( DLL* this )
{
   return this->first == NULL;
}

/**
 * @brief Devuelve el número actual de elementos en la lista.
 *
 * @param this Una lista.
 *
 * @return Devuelve el número actual de elementos en la lista.
 */
size_t DLL_Len( DLL* this )
{
   assert( this );

   return this->len;
}

/**
 * @brief Busca coincidencia con la cadena artista en la lista.
 *
 * @param this Una lista.
 * @param key El valor que se está buscando.
 *
 * @return Un apuntador al nodo donde se haya encontrado la primer coincidencia. Devuelve NULL en caso de no haber encontrado ninguna.
 */
Node* DLL_Find_nombreArtista( DLL* this, const char* key )
{
   assert( DLL_IsEmpty( this ) == false );

   Node* it = this->first;

   while( it != NULL ){
      if( strcmp( uppercase( it->unAlbum->nombreArtista ), uppercase( key ) ) == 0 ) break;
      it = it->next;
   }

   return it;
}

/**
 * @brief Busca coincidencia con la cadena album en la lista.
 *
 * @param this Una lista.
 * @param key El valor que se está buscando.
 *
 * @return Un apuntador al nodo donde se haya encontrado la primer coincidencia. Devuelve NULL en caso de no haber encontrado ninguna.
 */
Node* DLL_Find_nombreAlbum( DLL* this, const char* key )
{
   assert( DLL_IsEmpty( this ) == false );

   Node* it = this->first;

   while( it != NULL ){
      if( strcmp( uppercase( it->unAlbum->nombreAlbum ), uppercase( key ) ) == 0 ) break;
      it = it->next;
   }

   return it;
}

/**
 * @brief Busca el valor año en la lista.
 *
 * @param this Una lista.
 * @param key El valor que se está buscando.
 *
 * @return Un apuntador al nodo donde se haya encontrado la primer coincidencia. Devuelve NULL en caso de no haber encontrado ninguna.
 */
Node* DLL_Find_anhio( DLL* this, size_t key )
{
   assert( DLL_IsEmpty( this ) == false );

   Node* it = this->first;

   while( it != NULL ){
      if( it->unAlbum->anhio == key ) break;
      it = it->next;
   }

   return it;
}

/**
 * @brief Busca coincidencia con la cadena album en la lista.
 *
 * @param this Una lista.
 * @param key El valor que se está buscando.
 *
 * @return Un apuntador al nodo donde se haya encontrado la primer coincidencia. Devuelve NULL en caso de no haber encontrado ninguna.
 */
Node* DLL_Find_genero( DLL* this, const char* key )
{
   assert( DLL_IsEmpty( this ) == false );

   Node* it = this->first;

   while( it != NULL ){
      if( strcmp( uppercase( it->unAlbum->genero ), uppercase( key ) ) == 0 ) break;
      it = it->next;
   }

   return it;
}

/**
 * @brief Busca el valor numCanciones en la lista.
 *
 * @param this Una lista.
 * @param key El valor que se está buscando.
 *
 * @return Un apuntador al nodo donde se haya encontrado la primer coincidencia. Devuelve NULL en caso de no haber encontrado ninguna.
 */
Node* DLL_Find_numCanciones( DLL* this, size_t key )
{
   assert( DLL_IsEmpty( this ) == false );

   Node* it = this->first;

   while( it != NULL ){
      if( it->unAlbum->numCanciones == key ) break;
      it = it->next;
   }

   return it;
}

/**
 * @brief Busca coincidencia con la cadena nombreCancion en la lista.
 *
 * @param this Una lista.
 * @param key El valor que se está buscando.
 * @param index El valor donde se encuentra en el arreglo.
 *
 * @return Un apuntador al nodo donde se haya encontrado la primer coincidencia. Devuelve NULL en caso de no haber encontrado ninguna.
 */
Node* DLL_Find_nombreCancion( DLL* this, const char* key, size_t* index )
{
   assert( DLL_IsEmpty( this ) == false );

   Node* it = this->first;
   bool ban = false;

   while( it != NULL ){
      for ( size_t i = 0; i < it->unAlbum->numCanciones; i++ ){
         if( strcmp( uppercase( it->unAlbum->nombreCancion[i] ), uppercase( key ) ) == 0 ){
            ban = true;
            *index = i;
            break;
         }
      }
      if( ban == true ) break;
      it = it->next;
   }

   return it;
}

/**
 * @brief Busca el valor de calificacion de la cancion la lista.
 *
 * @param this Una lista.
 * @param key El valor que se está buscando.
 * @param index El valor donde se encuentra en el arreglo.
 *
 * @return Un apuntador al nodo donde se haya encontrado la primer coincidencia. Devuelve NULL en caso de no haber encontrado ninguna.
 */
Node* DLL_Find_calificacionCancion( DLL* this, size_t key, size_t* index )
{
   assert( DLL_IsEmpty( this ) == false );

   Node* it = this->first;
   index = 0;

   while( it != NULL ){
      for ( size_t i = 0; i < it->unAlbum->numCanciones; i++ ){
         if( it->unAlbum->calificacionCancion[i] == key ) break;
         else{
            it = it->next;
            index++;
         }
      }
   }

   return it;
}

/**
 * @brief Busca un valor en la lista.
 *
 * @param this Una lista.
 * @param key El valor que se está buscando.
 *
 * @return Un apuntador al nodo donde se haya encontrado la primer coincidencia. Devuelve NULL en caso de no haber encontrado ninguna.
 */
Node* DLL_Find_calificacionAlbum( DLL* this, size_t key )
{
   assert( DLL_IsEmpty( this ) == false );

   Node* it = this->first;

   while( it != NULL ){
      if( it->unAlbum->calificacionAlbum == key ) break;
      it = it->next;
   }

   if( key > 5 ){ // es decir, sin clasificación
      it = this->first;
      while( it != NULL ){
      if( it->unAlbum->calificacionAlbum > 5 ) break;
      it = it->next;
      }
   }

   return it;
}

/**
 * @brief Busca el valor de duración de la canción la lista.
 *
 * @param this Una lista.
 * @param key El valor que se está buscando.
 * @param index El valor donde se encuentra en el arreglo.
 *
 * @return Un apuntador al nodo donde se haya encontrado la primer coincidencia. Devuelve NULL en caso de no haber encontrado ninguna.
 */
Node* DLL_Find_duracion( DLL* this, size_t key, size_t* index )
{
   assert( DLL_IsEmpty( this ) == false );

   Node* it = this->first;
   index = 0;

   while( it != NULL ){
      for ( size_t i = 0; i < it->unAlbum->numCanciones; i++ ){
         if( it->unAlbum->duracion[i] == key ) break;
         else{
            it = it->next;
            index++;
         }
      }
   }

   return it;
}

/**
 * @brief Imprime la estructura de la lista. Es para uso de depuración, no de uso general.
 *
 * @param this Una lista.
 */
void DLL_PrintStructure( DLL* this )
{
   system("clear");
   printf( "Imprimir estructura de la lista (raw)\n" );

   if( DLL_IsEmpty( this ) ){
      fprintf( stderr, "\nLista vacía. Nada que mostrar.\n" );
   } else{

      Node* it = this->first;
      fprintf( stderr, "\nNil->" );

      while( it != NULL ){

         fprintf( stderr, "\nArtista: %s", it->unAlbum->nombreArtista );
         fprintf( stderr, "Álbum: %s", it->unAlbum->nombreAlbum );
         fprintf( stderr, "Calificación del álbum: %s", estrellas( it->unAlbum->calificacionAlbum ) );
         fprintf( stderr, "\nComentario: %s", it->unAlbum->comentario );
		   fprintf( stderr, "Año: %ld", it->unAlbum->anhio );
		   fprintf( stderr, "\nGénero: %s", it->unAlbum->genero );
         fprintf( stderr, "Número de pistas: %ld\n", it->unAlbum->numCanciones );
         for ( size_t i = 0; i < it->unAlbum->numCanciones; i++ ){
            fprintf( stderr, "\n" );
            fprintf( stderr, "Nombre de la canción %ld: %s", i+1, it->unAlbum->nombreCancion[i] );
            fprintf( stderr, "Calificación: %s", estrellas( it->unAlbum->calificacionCancion[i] ) );
            fprintf( stderr, "\nDuración: %s", ssm_2_hms( it->unAlbum->duracion[i] ) );
            fprintf( stderr, "\n" );
	      }
         fprintf( stderr, "\n***************************************************\n" );

         it = it->next;
      }
      fprintf( stderr, "Nil\n" );
   }
   Entorno_Mensaje_Continuar();
}

/**
 * @brief Captura informacion de teclado para insertar en la lista.
 *
 * @param this Una lista.
 */
void DLL_Leer( DLL* this )
{
   Entorno_Construir();
   int y = 2; // coordenada y
   char* buffer = (char*) malloc( TAM * sizeof( char ) );
   Album* unAlbum = (Album*) malloc( sizeof( Album ) );

   printf( "Agregar un álbum" ); y=y+2; Entorno_Gotoxy( 2, y );

   printf( "Ingrese el nombre del artista" ); Entorno_Gotoxy( 5, 41 );
   fgets( buffer, TAM, stdin ); Entorno_Limpiar_Terminal(); Entorno_Gotoxy( 2, y );
   unAlbum->nombreArtista = (char*) malloc( strlen( buffer ) * sizeof( char ) );
   strcpy( unAlbum->nombreArtista, buffer );
   printf( "Ingrese el nombre del artista: %s", unAlbum->nombreArtista ); y=y+1; Entorno_Gotoxy( 2, y );
   
   printf( "Ingrese el nombre del álbum" ); Entorno_Gotoxy( 5, 41 );
   fgets( buffer, TAM, stdin ); Entorno_Limpiar_Terminal(); Entorno_Gotoxy( 2, y );
   unAlbum->nombreAlbum = (char*) malloc( strlen( buffer ) * sizeof( char ) );
   strcpy( unAlbum->nombreAlbum, buffer );
   printf( "Ingrese el nombre del álbum: %s", unAlbum->nombreAlbum ); y=y+1; Entorno_Gotoxy( 2, y );

   printf( "Ingrese la calificación del álbum" ); Entorno_Gotoxy( 5, 41 );
   scanf( "%ld", &(unAlbum->calificacionAlbum) );
   fgets( buffer, TAM, stdin ); Entorno_Limpiar_Terminal(); Entorno_Gotoxy( 2, y );
   printf( "Ingrese la calificación del álbum: %s", estrellas(unAlbum->calificacionAlbum) ); y=y+1; Entorno_Gotoxy( 2, y );

   printf( "Ingrese un comentario del álbum" ); Entorno_Gotoxy( 5, 41 );
   fgets( buffer, TAM, stdin ); Entorno_Limpiar_Terminal(); Entorno_Gotoxy( 2, y );
   unAlbum->comentario = (char*) malloc( strlen( buffer ) * sizeof( char ) );
   strcpy( unAlbum->comentario, buffer );
   printf( "Ingrese un comentario del álbum: %s", unAlbum->comentario ); y=y+1; Entorno_Gotoxy( 2, y );

   printf( "Ingrese el año de lanzamiento" ); Entorno_Gotoxy( 5, 41 );
   scanf( "%ld", &(unAlbum->anhio) );
   fgets( buffer, TAM, stdin ); Entorno_Limpiar_Terminal(); Entorno_Gotoxy( 2, y );
   printf( "Ingrese el año de lanzamiento: %ld", unAlbum->anhio ); y=y+1; Entorno_Gotoxy( 2, y );

   printf( "Ingrese el género del álbum" ); Entorno_Gotoxy( 5, 41 );
   fgets( buffer, TAM, stdin ); Entorno_Limpiar_Terminal(); Entorno_Gotoxy( 2, y );
   unAlbum->genero = (char*) malloc( strlen( buffer ) * sizeof( char ) );
   strcpy( unAlbum->genero, buffer );
   printf( "Ingrese el género del álbum: %s", unAlbum->genero ); y=y+1; Entorno_Gotoxy( 2, y );

   printf( "Ingrese el número de pistas del álbum" ); Entorno_Gotoxy( 5, 41 );
   scanf( "%ld", &(unAlbum->numCanciones) );
   fgets( buffer, TAM, stdin ); Entorno_Limpiar_Terminal(); Entorno_Gotoxy( 2, y );
   printf( "Ingrese el número de pistas del álbum: %ld", unAlbum->numCanciones ); y=y+2; Entorno_Gotoxy( 2, y );
	
   unAlbum->nombreCancion = (char**) malloc( unAlbum->numCanciones * sizeof( char* ) );
   unAlbum->calificacionCancion = (size_t*) malloc( unAlbum->numCanciones * sizeof( size_t ) );
   unAlbum->duracion = (size_t*) malloc( unAlbum->numCanciones * sizeof( size_t ) );

	for ( size_t i = 0; i < unAlbum->numCanciones; i++ ){

      Entorno_Limpiar_Leer();
      y = 12;
      Entorno_Gotoxy( 2, y );

      printf( "Ingrese el nombre de la canción %ld", i+1 ); Entorno_Gotoxy( 5, 41 );
      fgets( buffer, TAM, stdin ); Entorno_Limpiar_Terminal(); Entorno_Gotoxy( 2, y );
      unAlbum->nombreCancion[i] = (char*) malloc( strlen( buffer ) * sizeof( char ) );
      strcpy( unAlbum->nombreCancion[i], buffer );
      printf( "Ingrese el nombre de la canción %ld: %s", i+1, unAlbum->nombreCancion[i] ); y=y+1; Entorno_Gotoxy( 2, y );

      printf( "Ingrese la calificación de la canción" ); Entorno_Gotoxy( 5, 41 );
      scanf( "%ld", &(unAlbum->calificacionCancion[i]) );
      fgets( buffer, TAM, stdin ); Entorno_Limpiar_Terminal(); Entorno_Gotoxy( 2, y );
      printf( "Ingrese la calificación de la canción: %s", estrellas(unAlbum->calificacionCancion[i]) ); y=y+1; Entorno_Gotoxy( 2, y );

      printf( "Ingrese la duración en segundos de la canción" ); Entorno_Gotoxy( 5, 41 );
      scanf( "%ld", &(unAlbum->duracion[i]) );
      fgets( buffer, TAM, stdin ); Entorno_Limpiar_Terminal();
	}

   Node* temp = new_node( unAlbum );

   DLL_Push_back( this, temp );

   Entorno_Mensaje_Exitosa();

   free ( buffer );
   buffer = NULL;
}

/**
 * @brief Duplica un elemento de la lista por coincidencia de captura en teclado
 *
 * @param this Una lista.
 */
void DLL_Duplicar( DLL* this )
{
   Entorno_Construir();
   int y = 2; // coordenada y
   char* buffer = (char*) malloc( TAM * sizeof( char ) );

   if( true == DLL_IsEmpty(this) ) Entorno_Lista_Vacia();
   else{
      printf( "Duplicar un álbum" ); y=y+2; Entorno_Gotoxy( 2, y );
      printf( "Ingrese el nombre del álbum que desea duplicar" ); y=y+2; Entorno_Gotoxy( 5, 41 );
      fgets( buffer, TAM, stdin );
      Entorno_Gotoxy( 2, y );

      if( NULL != DLL_Find_nombreAlbum( this, buffer ) ){
         printf( "Se duplicó con éxito el álbum %s", DLL_Find_nombreAlbum( this, buffer )->unAlbum->nombreAlbum );
         DLL_Clone( this, DLL_Find_nombreAlbum( this, buffer ) );
      } else{
         printf( "No se encontró el álbum %s", buffer );
      }
   }

   Entorno_Mensaje_Continuar();

   free( buffer );
   buffer = NULL;
}

/**
 * @brief Modifica un elemento de la lista por coincidencia de captura en teclado.
 *
 * @param this Una lista.
 */
void DLL_Modificar( DLL* this )
{
   Entorno_Construir();
   int y = 2; // coordenada y
   int opc = 0;
   char* buffer = (char*) malloc( TAM * sizeof( char ) );

   if( true == DLL_IsEmpty(this) ) Entorno_Lista_Vacia();
   else{
      printf( "Modificar un álbum" ); y=y+1; Entorno_Gotoxy( 2, y );
      printf( "Ingrese el nombre del álbum que desea modificar" ); y=y+2; Entorno_Gotoxy( 5, 41 );
      fgets( buffer, TAM, stdin );
      Entorno_Gotoxy( 2, y );

      if( NULL != DLL_Find_nombreAlbum( this, buffer ) ){
         printf( "Se encontró con éxito el álbum %s", DLL_Find_nombreAlbum( this, buffer )->unAlbum->nombreAlbum ); y=y+1; Entorno_Gotoxy( 2, y );
         Entorno_Mensaje_Continuar();
         DLL_Imprimir_Album( this, DLL_Find_nombreAlbum( this, buffer ) );
         Entorno_Mensaje_Continuar();
         opc = Entorno_Menu_3_3();
         Entorno_Construir();
         printf( "Modificar un álbum" ); y=y+2; Entorno_Gotoxy( 2, y );
         DLL_Imprimir_Album( this, DLL_Find_nombreAlbum( this, buffer ) );
         DLL_Edit( this, DLL_Find_nombreAlbum( this, buffer ), opc );
      } else{
         printf( "No se encontró el álbum %s", buffer );
         Entorno_Mensaje_Continuar();
      }
   }

   free( buffer );
   buffer = NULL;
}

/**
 * @brief Borra uno o más elementos de la lista por coincidencia de captura en teclado.
 *
 * @param this Una lista.
 * @param mode Selector.
 */
void DLL_Borrar( DLL* this, int mode )
{
   Entorno_Construir();
   int y = 2; // coordenada y
   char* buffer = (char*) malloc( TAM * sizeof( char ) );
   size_t number = 0;

   if( true == DLL_IsEmpty(this) ) mode = 0 ;

   if( mode == 4 ){
      printf( "Eliminar un álbum" ); y=y+2; Entorno_Gotoxy( 2, y );
      printf( "Ingrese el nombre del álbum que desea eliminar" ); y=y+2; Entorno_Gotoxy( 5, 41 );
      fgets( buffer, TAM, stdin );
      Entorno_Gotoxy( 2, y );

      if( NULL != DLL_Find_nombreAlbum( this, buffer ) ){
         printf( "Se eliminó con éxito el álbum %s", DLL_Find_nombreAlbum( this, buffer )->unAlbum->nombreAlbum );
         DLL_Erase( this, DLL_Find_nombreAlbum( this, buffer ) );
      } else{
         printf( "No se encontró el álbum %s", buffer );
      }
   } else if ( mode == 5 ){
      printf( "Eliminar los álbumes de un artista en común" ); y=y+2; Entorno_Gotoxy( 2, y );
      printf( "Ingrese el artista para eliminar sus álbumes asociados" ); y=y+2; Entorno_Gotoxy( 5, 41 );
      fgets( buffer, TAM, stdin );
      Entorno_Gotoxy( 2, y );

      if( NULL != DLL_Find_nombreArtista( this, buffer ) ){
         printf( "Se encontró al artista %s", DLL_Find_nombreArtista( this, buffer )->unAlbum->nombreArtista ); y=y+2; Entorno_Gotoxy( 2, y );
         while( NULL != DLL_Find_nombreArtista( this, buffer ) ){
            printf( "Se eliminó con éxito el álbum %s", DLL_Find_nombreArtista( this, buffer )->unAlbum->nombreAlbum ); y=y+1; Entorno_Gotoxy( 2, y );
            DLL_Erase( this, DLL_Find_nombreArtista( this, buffer ) );
         }
      } else{
         printf( "No se encontró al artista %s", buffer );
      }
   } else if ( mode == 6 ){
      printf( "Eliminar los álbumes de un género en común" ); y=y+2; Entorno_Gotoxy( 2, y );
      printf( "Ingrese el género para eliminar los álbumes asociados" ); y=y+2; Entorno_Gotoxy( 5, 41 );
      fgets( buffer, TAM, stdin );
      Entorno_Gotoxy( 2, y );

      if( NULL != DLL_Find_genero( this, buffer ) ){
         printf( "Se encontró al ǵenero %s", DLL_Find_genero( this, buffer )->unAlbum->genero ); y=y+2; Entorno_Gotoxy( 2, y );
         while( NULL != DLL_Find_genero( this, buffer ) ){
            printf( "Se eliminó con éxito el álbum %s", DLL_Find_genero( this, buffer )->unAlbum->nombreAlbum ); y=y+1; Entorno_Gotoxy( 2, y );
            DLL_Erase( this, DLL_Find_genero( this, buffer ) );
            if( true == DLL_IsEmpty(this) )
            break;
         }
      } else{
         printf( "No se encontró el género %s", buffer );
      }
   } else if ( mode == 7 ){
      printf( "Eliminar los álbumes de un año en común" ); y=y+2; Entorno_Gotoxy( 2, y );
      printf( "Ingrese el año para eliminar los álbumes asociados" ); y=y+2; Entorno_Gotoxy( 5, 41 );
      scanf( "%ld", &number );
      fgets( buffer, TAM, stdin );
      Entorno_Gotoxy( 2, y );

      if( NULL != DLL_Find_anhio( this, number ) ){
         printf( "Se encontró el año %ld", number ); y=y+2; Entorno_Gotoxy( 2, y );
         while( NULL != DLL_Find_anhio( this, number ) ){
            printf( "Se eliminó con éxito el álbum %s", DLL_Find_anhio( this, number )->unAlbum->nombreAlbum ); y=y+1; Entorno_Gotoxy( 2, y );
            DLL_Erase( this, DLL_Find_anhio( this, number ) );
         }
      } else{
         printf( "No se encontró el año %ld", number );
      }
   } else if ( mode == 8 ){
      printf( "Eliminar los álbumes de una calificación en común" ); y=y+2; Entorno_Gotoxy( 2, y );
      printf( "Ingrese una calificación para eliminar los álbumes asociados" ); y=y+1; Entorno_Gotoxy( 2, y );
      printf( "(Un numero mayor que 5 cuenta como \"not rated\")" ); y=y+2; Entorno_Gotoxy( 5, 41 );
      scanf( "%ld", &number );
      fgets( buffer, TAM, stdin );
      Entorno_Gotoxy( 2, y );

      if( NULL != DLL_Find_calificacionAlbum( this, number ) ){
         printf( "Existen álbumes con clasificación %s", estrellas( number ) ); y=y+2; Entorno_Gotoxy( 2, y );
         while( NULL != DLL_Find_calificacionAlbum( this, number ) ){
            printf( "Se eliminó con éxito el álbum %s", DLL_Find_calificacionAlbum( this, number )->unAlbum->nombreAlbum ); y=y+1; Entorno_Gotoxy( 2, y );
            DLL_Erase( this, DLL_Find_calificacionAlbum( this, number ) );
         }
      } else{
         printf( "No existen elementos con clasificación %s", estrellas( number ) );
      }
   } else if ( mode == 9 ){
      printf( "Eliminar TODOS los álbumes" ); y=y+2; Entorno_Gotoxy( 2, y );

      if( true == DLL_IsEmpty( this ) ){
         printf( "La biblioteca ya estaba vacía, no se realizó ninguna acción" );
      } else{
         DLL_MakeEmpty( this );
         printf( "La biblioteca ha quedado sin elementos :)" );
      }
   } else{
      Entorno_Lista_Vacia();
   }

   Entorno_Mensaje_Continuar();

   free( buffer );
   buffer = NULL;
}

/**
 * @brief Guarda la lista en un archivo de persistencia.
 *
 * @param this Una lista.
 */
void DLL_Guardar ( DLL* this )
{
   FILE* biblioteca;
   Node* it = this->first;

   biblioteca = fopen( "biblioteca.txt", "w+" );
	
	if ( it == NULL ){
      fclose( biblioteca );
   } else{
      while ( it != NULL ){
         fprintf( biblioteca, "%s", it->unAlbum->nombreArtista );
         fprintf( biblioteca, "%s", it->unAlbum->nombreAlbum );
         fprintf( biblioteca, "%ld", it->unAlbum->calificacionAlbum );
         fprintf( biblioteca, "%s", "\n" );
         fprintf( biblioteca, "%s", it->unAlbum->comentario );
		   fprintf( biblioteca, "%ld", it->unAlbum->anhio );
         fprintf( biblioteca, "%s", "\n" );
		   fprintf( biblioteca, "%s", it->unAlbum->genero );
         fprintf( biblioteca, "%ld", it->unAlbum->numCanciones );
         fprintf( biblioteca, "%s", "\n" );
         for ( size_t i = 0; i < it->unAlbum->numCanciones; i++ ){
            fprintf( biblioteca, "%s", "\n" );
            fprintf( biblioteca, "%s", it->unAlbum->nombreCancion[i] );
            fprintf( biblioteca, "%ld", it->unAlbum->calificacionCancion[i] );
            fprintf( biblioteca, "%s", "\n" );
            fprintf( biblioteca, "%ld", it->unAlbum->duracion[i] );
            fprintf( biblioteca, "%s", "\n" );
	      }
         fprintf( biblioteca, "%s", "\n" );
		   fprintf( biblioteca, "%s", "***************************************************" );
         fprintf( biblioteca, "%s", "\n" );
         fprintf( biblioteca, "%s", "\n" );
			
			it = it->next;
		}// Fin del while para guardar toda la informacion
		
		fprintf( biblioteca, "%s", "@**FIN**" );
		fclose( biblioteca );
	}
}

/**
 * @brief Carga la lista de un archivo de persistencia.
 *
 * @param this Una lista.
 */
void DLL_Cargar ( DLL* this )
{
   FILE* biblioteca;
   char* buffer = (char*) malloc( TAM * sizeof( char ) );
   bool ban = true;

   biblioteca = fopen( "biblioteca.txt", "r+" );
   fseek( biblioteca, 0, SEEK_END );

   if( ftell( biblioteca ) == 0 ) fclose( biblioteca ); //El archivo esta vacio
   else{
      rewind( biblioteca );
      while( ban == true ){
         fgets( buffer, TAM, biblioteca );
         if ( buffer[0] == '@' ){ //Llegamos al final del fichero
            fclose( biblioteca );
            ban = false;
         } else{
            Album* unAlbum = (Album*) malloc( sizeof( Album ) );

            unAlbum->nombreArtista = (char*) malloc( strlen( buffer ) * sizeof( char ) );
            strcpy( unAlbum->nombreArtista, buffer );

            fgets( buffer, TAM, biblioteca );
            unAlbum->nombreAlbum = (char*) malloc( strlen( buffer ) * sizeof( char ) );
            strcpy( unAlbum->nombreAlbum, buffer );

            fscanf( biblioteca, "%ld", &(unAlbum->calificacionAlbum) );
            fgets( buffer, TAM, biblioteca );

            fgets( buffer, TAM, biblioteca );
            unAlbum->comentario = (char*) malloc( strlen( buffer ) * sizeof( char ) );
            strcpy( unAlbum->comentario, buffer );
	
            fscanf( biblioteca, "%ld", &(unAlbum->anhio) );
            fgets( buffer, TAM, biblioteca );
   
            fgets( buffer, TAM, biblioteca );
            unAlbum->genero = (char*) malloc( strlen( buffer ) * sizeof( char ) );
            strcpy( unAlbum->genero, buffer );
	
	         fscanf( biblioteca, "%ld", &(unAlbum->numCanciones) );
            fgets( buffer, TAM, biblioteca );
   
            unAlbum->nombreCancion = (char**) malloc( unAlbum->numCanciones * sizeof( char* ) );
            unAlbum->calificacionCancion = (size_t*) malloc( unAlbum->numCanciones * sizeof( size_t ) );
            unAlbum->duracion = (size_t*) malloc( unAlbum->numCanciones * sizeof( size_t ) );

	         for ( size_t i = 0; i < unAlbum->numCanciones; i++ ){
               fgets( buffer, TAM, biblioteca );
		         fgets( buffer, TAM, biblioteca );
               unAlbum->nombreCancion[i] = (char*) malloc( strlen( buffer ) * sizeof( char ) );
               strcpy( unAlbum->nombreCancion[i], buffer );

               fscanf( biblioteca, "%ld", &(unAlbum->calificacionCancion[i]) );
               fgets( buffer, TAM, biblioteca );

               fscanf( biblioteca, "%ld", &(unAlbum->duracion[i]) );
               fgets( buffer, TAM, biblioteca );
	         }

            fgets( buffer, TAM, biblioteca );
            fgets( buffer, TAM, biblioteca );
            fgets( buffer, TAM, biblioteca );

            Node* temp = new_node( unAlbum );

            DLL_Push_back( this, temp );
         }
      }
   }
   free ( buffer );
   buffer = NULL;
}

/**
 * @brief Imprime informacion seleccionada y organizada de la lista.
 *
 * @param this Una lista.
 * @param field Campo.
 * @param rating Puntaje.
 */
void DLL_Imprimir( DLL* this, int field, int rating )
{
   Entorno_Construir();
   int k = 0; // indice para el arreglo
   int n = 0; // cantidad total de elementos
   char** arr = NULL;
   Node* it = this->first;

   if( field == 1 ){
      printf( "Todos los artistas" );
      n = this->len;
      arr = (char**) malloc( n * sizeof( char* ) );
      
      while( it != NULL ){
         arr[k] = it->unAlbum->nombreArtista;
         it = it->next;
         k++;
      }
   } else if( field == 2 ){
      printf( "Todas las canciones" );

      while( it != NULL ){
         n = n + it->unAlbum->numCanciones;
         it = it->next;
      }

      arr = (char**) malloc( n * sizeof( char* ) );
      it = this->first;

      while( it != NULL ){
         for( size_t i = 0; i < it->unAlbum->numCanciones; i++ ){
            arr[k] = it->unAlbum->nombreCancion[i];
            k++;
         }
         it = it->next;
      }
      qsort( arr, n, sizeof(const char*), comparador);
   } else if( field == 3 ){
      printf( "Todos los álbumes" );

      n = this->len;
      arr = (char**) malloc( n * sizeof( char* ) );
      
      while( it != NULL ){
         arr[k] = it->unAlbum->nombreAlbum;
         it = it->next;
         k++;
      }
   } else if( field == 4 ){
      printf( "Todos los géneros" );

      n = this->len;
      arr = (char**) malloc( n * sizeof( char* ) );
      
      while( it != NULL ){
         arr[k] = it->unAlbum->genero;
         it = it->next;
         k++;
      }
   } else if( field == 5 ){
      invertRating ( &rating );
      printf( "Todos los álbumes de %s estrellas", estrellas( rating ) );

      while( it != NULL ){
         if( it->unAlbum->calificacionAlbum == rating ) n++;
         it = it->next;
      }

      arr = (char**) malloc( n * sizeof( char* ) );
      it = this->first;
      
      while( it != NULL ){
         if( it->unAlbum->calificacionAlbum == rating ){
            arr[k] = it->unAlbum->nombreAlbum;
            k++;
         }
         it = it->next;
      }
   } else{
      invertRating ( &rating );
      printf( "Todas las canciones de %s estrellas", estrellas( rating ) );

      while( it != NULL ){
         for( size_t i = 0; i < it->unAlbum->numCanciones; i++ ){
            if( it->unAlbum->calificacionCancion[i] == rating ) n++;
         }
         it = it->next;
      }

      arr = (char**) malloc( n * sizeof( char* ) );
      it = this->first;

      while( it != NULL ){
         for( size_t i = 0; i < it->unAlbum->numCanciones; i++ ){
            if( it->unAlbum->calificacionCancion[i] == rating ){
               arr[k] = it->unAlbum->nombreCancion[i];
               k++;
            }
         }
         it = it->next;
      }
   }

   qsort( arr, n, sizeof(const char*), comparador);

   Entorno_Imprimir_Pagina( arr, n );
   Entorno_Mensaje_Continuar();

   free( arr );
   arr = NULL;
}

/**
 * @brief Imprime informacion seleccionada y organizada de un elemento.
 *
 * @param this Una lista.
 * @param item Elemento.
 */
void DLL_Imprimir_Album( DLL* this, Node* item )
{
   Entorno_Construir();
   int y = 2; // coordenada y

   printf( "%s", item->unAlbum->nombreArtista ); Entorno_Gotoxy( strlen( item->unAlbum->nombreArtista ) + 1, y );
   printf( " - %s", item->unAlbum->nombreAlbum ); Entorno_Gotoxy( strlen( item->unAlbum->nombreArtista ) + strlen( item->unAlbum->nombreAlbum ) + 4, y );
   printf( "(%ld)", item->unAlbum->anhio ); y=y+2; Entorno_Gotoxy( 2, y );

   for( size_t i = 0; i < item->unAlbum->numCanciones; i++ ){
      printf( "%ld %s", i+1, item->unAlbum->nombreCancion[i] ); y=y+1; Entorno_Gotoxy( 2, y );
   }
   y=y+2; Entorno_Gotoxy( 2, y );
}

/**
 * @brief Busca un elementos de la lista por coincidencia de captura en teclado.
 *
 * @param this Una lista.
 * @param mode Selector.
 */
void DLL_Busqueda( DLL* this, int mode )
{
   Entorno_Construir();
   int y = 2; // coordenada y
   char* buffer = (char*) malloc( TAM * sizeof( char ) );
   Node* temp = this->first;

   if( true == DLL_IsEmpty(this) ) mode = 0;

   if( mode == 1 ){
      printf( "Buscar un artista" ); y=y+2; Entorno_Gotoxy( 2, y );
      printf( "Ingrese el nombre del artista que deseas encontrar" );
      Entorno_Gotoxy( 5, 41 );
      fgets( buffer, TAM, stdin );
      Entorno_Gotoxy( 2, 6 );

      if( NULL != DLL_Find_nombreArtista( this, buffer ) ){
         temp = DLL_Find_nombreArtista( this, buffer );
         printf( "Se encontró al artista %s", temp->unAlbum->nombreArtista );
      } else{
         printf( "No se encontró al artista %s", buffer );
      }
   } else if( mode == 2 ){
      printf( "Buscar una canción" ); y=y+2; Entorno_Gotoxy( 2, y );
      printf( "Ingrese el nombre de la canción que deseas encontrar" );
      Entorno_Gotoxy( 5, 41 );
      fgets( buffer, TAM, stdin );
      Entorno_Gotoxy( 2, 6 );
      
      size_t index = 0;

      if( NULL != DLL_Find_nombreCancion( this, buffer, &index ) ){
         temp = DLL_Find_nombreCancion( this, buffer, &index );
         printf( "Se encontró la canción %s", temp->unAlbum->nombreCancion[index] );
      } else{
         printf( "No se encontró la canción %s", buffer );
      }
   } else if( mode == 3 ){
      printf( "Buscar un álbum" ); y=y+2; Entorno_Gotoxy( 2, y );
      printf( "Ingrese el nombre del álbum que deseas encontrar" );
      Entorno_Gotoxy( 5, 41 );
      fgets( buffer, TAM, stdin );
      Entorno_Gotoxy( 2, 6 );

      if( NULL != DLL_Find_nombreAlbum( this, buffer ) ){
         temp = DLL_Find_nombreAlbum( this, buffer );
         printf( "Se encontró el álbum %s", temp->unAlbum->nombreAlbum );
      } else{
         printf( "No se encontró el álbum %s", buffer );
      }
   } else if( mode == 4 ){
      printf( "Buscar un género" ); y=y+2; Entorno_Gotoxy( 2, y );
      printf( "Ingrese el nombre del género que deseas encontrar" );
      Entorno_Gotoxy( 5, 41 );
      fgets( buffer, TAM, stdin );
      Entorno_Gotoxy( 2, 6 );

      if( NULL != DLL_Find_genero( this, buffer ) ){
         temp = DLL_Find_genero( this, buffer );
         printf( "Se encontró el género %s", temp->unAlbum->genero );
      } else{
         printf( "No se encontró el %s", buffer );
      }
   } else{
      Entorno_Lista_Vacia();
   }
   Entorno_Mensaje_Continuar();

   free( buffer );
   buffer = NULL;
}

/**
 * @brief Emula la reproducción de una lista.
 *
 * @param this Una lista.
 */
void DLL_Reproducir( DLL* this )
{
   struct timeval lapse;
   bool expiration;
   bool condition;
   int opc = 0;
   int y = 2;
   
   Entorno_Gotoxy( 77,2 );
   printf( "%s", this->cursor->unAlbum->nombreCancion[0] ); y=y+1; Entorno_Gotoxy( 78,y );
   printf( "%s", this->cursor->unAlbum->nombreArtista ); y=y+1; Entorno_Gotoxy( 76,y );
   printf( "%s", this->cursor->unAlbum->nombreAlbum ); y=y+1; Entorno_Gotoxy( 81,y );
   printf( "%s", this->cursor->unAlbum->comentario ); y=y+1; Entorno_Gotoxy( 74,y );
   printf( "%ld", this->cursor->unAlbum->anhio ); y=y+1; Entorno_Gotoxy( 77,y );
   printf( "%s", this->cursor->unAlbum->genero ); y=y+1; Entorno_Gotoxy( 86,y );
   printf( "%d", 1 ); y=y+1; Entorno_Gotoxy( 95,y );
   printf( "%s", estrellas( this->cursor->unAlbum->calificacionCancion[0] ) ); y=y+1; Entorno_Gotoxy( 93,y );
   printf( "%s", estrellas( this->cursor->unAlbum->calificacionAlbum ) ); y=y+2; Entorno_Gotoxy( 93,y );
   printf( "%s", "Reproduciendo" ); Entorno_Gotoxy( 125,y );
   printf( "%s", ssm_2_hms( this->cursor->unAlbum->duracion[0] ) );
   
   fd_set rfds;
   FD_ZERO( &rfds );
   FD_SET( 0, &rfds );

   for ( int i = 0; i < this->cursor->unAlbum->duracion[0] ; i++ ){
      lapse.tv_sec = 1;
      lapse.tv_usec = 0;
      do{
         expiration = select( 1, &rfds, NULL, NULL, &lapse );
         if( expiration == 0 ){ // reset the entry
            FD_ZERO( &rfds );
            FD_SET( 0, &rfds );
            Entorno_Gotoxy( 70,y );
            printf( "%s", ssm_2_hms( i+1 ) );
            Entorno_Puntero_Terminal();
         } else{
            // do something and then put condition true
            opc = Entorno_Mensaje_Experimento();
            if( opc == true )
            condition = true;
         }
      }while ( expiration != 0 && errno == EINTR );
      if ( condition == true ) break;
   }
}