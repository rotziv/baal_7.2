/*Copyright (C) 
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * 2021 - Chavez Aquiagual Victor Manuel
 */

 // ("Basado en el entorno de MOC - music on console - console audio player for Linux/UNIX")

#ifndef  ENTORNO_INC
#define  ENTORNO_INC

#define WIDTH 132 // para el largo de la consola
#define HEIGHT 43 // para el ancho de la consola

#define RESET "\x1B[0m"
#define BACKGROUND  "\x1B[37;40m"

#include <stdlib.h>
// para malloc and friends y size_t

#include <stdbool.h>
// para bool and friends

#include <stdio.h>
// para fprintf()

void Entorno_Gotoxy( int x, int y );
void Entorno_Mensaje_Continuar( void );
int Entorno_Mensaje_Experimento( void );
void Entorno_Mensaje_Next( void );
void Entorno_Mensaje_Exitosa( void );
void Entorno_Mensaje_Modificacion( void );
void Entorno_Mensaje_Error( void );
void Entorno_Limpiar_Pantalla( void );
void Entorno_Limpiar_Pagina( void );
void Entorno_Limpiar_Leer( void );
void Entorno_Limpiar_Terminal( void );
void Entorno_Construir( void );
void Entorno_Pagina( int desde, int hasta );
void Entorno_Imprimir_Pagina( char** arr, int elems );
void Entorno_Lista_Vacia( void );
void Entorno_Puntero_Terminal( void );
void Entorno_Menu_Tutorial( void );
size_t Entorno_Menu_Principal( void );
size_t Entorno_Menu_1( void );
size_t Entorno_Menu_2( void );
size_t Entorno_Menu_3( void );
size_t Entorno_Menu_1_5y6( char* word );
size_t Entorno_Menu_3_3( void );
size_t Entorno_Menu_4( void );

#endif   /* ----- #ifndef ENTORNO_INC  ----- */
