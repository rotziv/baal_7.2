/*Copyright (C) 
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * 2021 - Chavez Aquiagual Victor Manuel
 */

 // ("Basado en el entorno de MOC - music on console - console audio player for Linux/UNIX")

#include "Entorno.h"

static bool validarRango ( size_t opc, size_t max )
{
	bool ban = true;
	
	if ( opc < 1 || opc > max )
	ban = false;

	return ban;
}

/**
 * @brief Mueve el puntero a una ubicacion en pantalla.
 *
 * @param x Coordenada.
 * @param y Coordenada.
 */
void Entorno_Gotoxy( int x, int y )
{
    printf( "%c[%d;%df", 0x1B, y, x );
}

/**
 * @brief Muestra mensaje de continuar en area de terminal.
 */
void Entorno_Mensaje_Continuar( void )
{
   char myChar = 0;

   Entorno_Gotoxy(5,41);
   printf( "Presione [ENTER] para continuar" );

   while ( myChar != '\n' && myChar != '\r' ){ 
      myChar = getchar(); 
   }
}

/**
 * @brief Muestra mensaje de experimento en area de terminal.
 *
 * @return El numero 1.
 */
int Entorno_Mensaje_Experimento( void )
{
   char myChar = 0;

   Entorno_Gotoxy(5,41);
   printf( "Presione [ENTER] para salir de esta función experimental" );

   while ( myChar != '\n' && myChar != '\r' ){ 
      myChar = getchar(); 
   }

   return 1;
}

/**
 * @brief Muestra mensaje de pagina siguiente en area de terminal.
 */
void Entorno_Mensaje_Next( void )
{
   char myChar = 0;

   Entorno_Gotoxy(5,41);
   printf( "Presione [ENTER] para ir a la siguiente página" );

   while ( myChar != '\n' && myChar != '\r' ){ 
      myChar = getchar(); 
   }
}

/**
 * @brief Muestra mensaje exitoso en area de terminal.
 */
void Entorno_Mensaje_Exitosa( void )
{
   char myChar = 0;

   Entorno_Gotoxy(5,41);
   printf( "Operación exitosa, presione [ENTER] para continuar" );

   while ( myChar != '\n' && myChar != '\r' ){ 
      myChar = getchar(); 
   }
}

/**
 * @brief Muestra mensaje modificación en area de terminal.
 */
void Entorno_Mensaje_Modificacion( void )
{
   char myChar = 0;

   Entorno_Gotoxy(5,41);
   printf( "Modificación exitosa, presione [ENTER] para continuar" );

   while ( myChar != '\n' && myChar != '\r' ){ 
      myChar = getchar(); 
   }
}

/**
 * @brief Muestra mensaje error en area de terminal.
 */
void Entorno_Mensaje_Error( void )
{
   char myChar = 0;

   Entorno_Gotoxy(5,41);
   printf( "Error, la opción no es válida! Presione [ENTER] para continuar" );

   while ( myChar != '\n' && myChar != '\r' ){ 
      myChar = getchar(); 
   }
}

/**
 * @brief Limpia area efectiva global.
 */
void Entorno_Limpiar_Pantalla( void )
{
   system("clear");
   for ( int j = 0; j < HEIGHT; j++ ){
      for ( int i = 0; i < WIDTH; i++ ){
         Entorno_Gotoxy( i+1, j+1 );
         printf( BACKGROUND " " );
      }
   }
}

/**
 * @brief Limpia area efectiva de pagina.
 */
void Entorno_Limpiar_Pagina( void )
{
   for ( int j = 3; j < 37; j++ ){
      for ( int i = 1; i < 65; i++ ){
         Entorno_Gotoxy( i+1, j+1 );
         printf( BACKGROUND " " );
      }
   }
}

/**
 * @brief Limpia area efectiva al capturar.
 */
void Entorno_Limpiar_Leer( void )
{
   for ( int j = 11; j < 37; j++ ){
      for ( int i = 1; i < 65; i++ ){
         Entorno_Gotoxy( i+1, j+1 );
         printf( BACKGROUND " " );
      }
   }
}

/**
 * @brief Limpia area efectiva terminal.
 */
void Entorno_Limpiar_Terminal( void )
{
   Entorno_Gotoxy( 1, 41 );
   printf( "│ $                                                                                                                                │" );
}

/**
 * @brief Construye entorno completo.
 */
void Entorno_Construir( void )
{
   int y = 1;
   Entorno_Limpiar_Pantalla();
   Entorno_Gotoxy( 1, y );
   printf( "┌───────────────────────┤Baal Player 7.0├────────────────────────┐┌─────────────────────────┤Pista Actual├─────────────────────────┐" ); y++; Entorno_Gotoxy( 1, y );
   printf( "│                                                                ││ Nombre:                                                        │" ); y++; Entorno_Gotoxy( 1, y );
   printf( "│                                                                ││ Artista:                                                       │" ); y++; Entorno_Gotoxy( 1, y );
   printf( "│                                                                ││ Álbum:                                                         │" ); y++; Entorno_Gotoxy( 1, y );
   printf( "│                                                                ││ Comentario:                                                    │" ); y++; Entorno_Gotoxy( 1, y );
   printf( "│                                                                ││ Año:                                                           │" ); y++; Entorno_Gotoxy( 1, y );
   printf( "│                                                                ││ Género:                                                        │" ); y++; Entorno_Gotoxy( 1, y );
   printf( "│                                                                ││ Número de pista:                                               │" ); y++; Entorno_Gotoxy( 1, y );
   printf( "│                                                                ││ Calificación de la pista:                                      │" ); y++; Entorno_Gotoxy( 1, y );
   printf( "│                                                                ││ Calificación del álbum:                                        │" ); y++; Entorno_Gotoxy( 1, y );
   printf( "│                                                                ││                                                                │" ); y++; Entorno_Gotoxy( 1, y );
   printf( "│                                                                ││ [00:00]                    Detenido                    [--:--] │" ); y++; Entorno_Gotoxy( 1, y );
   printf( "│                                                                ││                                                                │" ); y++; Entorno_Gotoxy( 1, y );
   printf( "│                                                                │├───────────────────────────┤Playlist├───────────────────────────┤" ); y++; Entorno_Gotoxy( 1, y );
   for ( int i = y; i < 39; i++ ){
   printf( "│                                                                ││                                                                │" ); y++; Entorno_Gotoxy( 1, y );
   }
   printf( "├────┤ Página: 1 / 1           ├───────────────────────────────────────────────────────────────────────────────────────────────────┤" ); y++; Entorno_Gotoxy( 1, y );
   printf( "│                                                                                                                                  │" ); y++; Entorno_Gotoxy( 1, y );
   printf( "│ $                                                                                                                                │" ); y++; Entorno_Gotoxy( 1, y );
   printf( "│                                                                                                                                  │" ); y++; Entorno_Gotoxy( 1, y );
   printf( "└───────────────────────┤ Universidad Nacional Autónoma de México - Instituto Politécnico Nacional ├───────────────────────────────┘" ); Entorno_Gotoxy(2,2);
}

/**
 * @brief Muestra barra de paginas.
 *
 * @param desde Valor inicial.
 * @param hasta Valor final.
 */
void Entorno_Barra_Paginas( int desde, int hasta )
{
   Entorno_Gotoxy( 1, 39 );
   printf( "├────┤ Página: %d / %d           ├───────────────────────────────────────────────────────────────────────────────────────────────────┤", desde, hasta );
}

/**
 * @brief Imprime un arreglo ordenado en secciones de páginas.
 *
 * @param arr Arreglo de elementos.
 * @param elems Cantidad de elementos.
 */
void Entorno_Imprimir_Pagina( char** arr, int elems )
{
   int y = 4;
   int k = 0;
   int n = elems;
   Entorno_Gotoxy( 2, y );

   if( n == 0 ) printf( "No se encontraron elementos" );

   if( n <= 34 ){ // si los elementos caben en una sola la página
      for( int i = 0; i < n; i++ ){  
         printf( "%d %s", k+1, arr[k] ); y=y+1; Entorno_Gotoxy( 2, y );
         k++;
      }
   } else{ // si los elementos no caben en una sola página

      int totalPages = (int)(( n / 33 ) + 0.5 ); //obtenemos el numero de paginas redondeado
      int remainingItems = n; // obtener los items restantes

      for( int j = 0; j < totalPages; j++ ){
         y = 4;
         Entorno_Limpiar_Pagina();
         Entorno_Limpiar_Terminal();
         Entorno_Gotoxy( 2, y );

         for( int i = 0; i < 33; i++ ){
            printf( "%d %s", k+1, arr[k] ); y=y+1; Entorno_Gotoxy( 2, y );
            k++;
         }

         remainingItems = remainingItems - 33; // restamos los items recien impresos
         printf( "+%d remaining items", remainingItems ); y=y+1; Entorno_Gotoxy( 2, y );

         if( ( n % 33 ) != 0 ){
            totalPages = totalPages + 1;
            Entorno_Barra_Paginas( j+1, totalPages );
            totalPages = totalPages - 1;
         }
         Entorno_Mensaje_Next();
      }

      if( remainingItems > 0 ){
         y = 4;
         Entorno_Limpiar_Pagina();
         Entorno_Limpiar_Terminal();
         Entorno_Gotoxy( 2, y );

         for( int i = 0; i < remainingItems; i++ ){
            printf( "%d %s", k+1, arr[k] ); y=y+1; Entorno_Gotoxy( 2, y );
            k++;
         }

         Entorno_Barra_Paginas( totalPages + 1, totalPages + 1 );
      }
   }
}

/**
 * @brief Muestra mensaje de lista vacía.
 */
void Entorno_Lista_Vacia( void )
{
   Entorno_Construir();
   int y = 2; // coordenada y
   printf( "Biblioteca vacía :(" ); y=y+2; Entorno_Gotoxy( 2, y );
   printf( "Imposible continuar porque la biblioteca no tiene elementos" );
}

/**
 * @brief Forza al puntero a permanecer en area de terminal.
 */
void Entorno_Puntero_Terminal( void )
{
   Entorno_Gotoxy( 5, 40 );
   printf("\n");
   fprintf( stderr, "│ $ " );
}

/**
 * @brief Construye Menú Tutorial.
 */
void Entorno_Menu_Tutorial( void )
{
   int y = 2;

	Entorno_Construir();
   Entorno_Gotoxy( 2, y );
      
	printf( "Tutorial para usar el reproductor *¡¡PROXIMAMENTE!!*" ); y=y+2; Entorno_Gotoxy( 2, y );
   printf( "Introduce las siguientes letras en la terminal" ); y=y+2; Entorno_Gotoxy( 2, y );

	printf( "\"P\" play/pause" ); y=y+1; Entorno_Gotoxy( 2, y );
	printf( "\"B\" pista previa" ); y=y+1; Entorno_Gotoxy( 2, y );
	printf( "\"F\" pista siguiente" ); y=y+1; Entorno_Gotoxy( 2, y );
	printf( "\"S\" stop" ); y=y+2; Entorno_Gotoxy( 2, y );
   
   printf( "Fin del tutorial" );

   Entorno_Mensaje_Continuar();
}

/**
 * @brief Construye Menú Principal.
 *
 * @return Opción validada.
 */
size_t Entorno_Menu_Principal( void )
{
   int y = 2;
	size_t opc;

	do{
      y = 2;
      Entorno_Construir();
      Entorno_Gotoxy( 2, y );
		printf( "Bienvenido a tu biblioteca de música" ); y=y+2; Entorno_Gotoxy( 2, y );

		printf( "%d Explorar la biblioteca", y-3 ); y=y+1; Entorno_Gotoxy( 2, y );
		printf( "%d Buscar en la biblioteca", y-3 ); y=y+1; Entorno_Gotoxy( 2, y );
      printf( "%d Modificar la biblioteca", y-3 ); y=y+1; Entorno_Gotoxy( 2, y );
		printf( "%d Opciones de depuración", y-3 ); y=y+1; Entorno_Gotoxy( 2, y );
      printf( "%d Reproducir la biblioteca *EXPERIMENTAL*", y-3 ); y=y+1; Entorno_Gotoxy( 2, y );
		printf( "%d Salir", y-3 ); y=y+2; Entorno_Gotoxy( 2, y );

      printf( "Por favor, seleccione una opcion" );

      Entorno_Gotoxy(5,41);
		scanf( "%ld", &opc );
      getchar();

      if ( validarRango( opc, y-5 ) == false )
      Entorno_Mensaje_Error();

	}while( validarRango( opc, y-5 ) == false );
	
	return opc;
}

/**
 * @brief Construye Menú 1.
 *
 * @return Opción validada.
 */
size_t Entorno_Menu_1( void )
{
   int y = 2;
	size_t opc;

	do{
      y = 2;
      Entorno_Construir();
      Entorno_Gotoxy( 2, y );
		printf( "Explorar la biblioteca" ); y=y+2; Entorno_Gotoxy( 2, y );

		printf( "%d Mostrar todos los artistas", y-3 ); y=y+1; Entorno_Gotoxy( 2, y );
		printf( "%d Mostrar todas las canciones", y-3 ); y=y+1; Entorno_Gotoxy( 2, y );
		printf( "%d Mostrar todos los álbumes", y-3 ); y=y+1; Entorno_Gotoxy( 2, y );
		printf( "%d Mostrar todos los géneros", y-3 ); y=y+1; Entorno_Gotoxy( 2, y );
      printf( "%d Mostrar por calificación de álbum", y-3 ); y=y+1; Entorno_Gotoxy( 2, y );
      printf( "%d Mostrar por calificación de pistas", y-3 ); y=y+1; Entorno_Gotoxy( 2, y );
		printf( "%d Regresar al menú anterior", y-3 ); y=y+2; Entorno_Gotoxy( 2, y );

      printf( "Por favor, seleccione una opcion" );

      Entorno_Gotoxy(5,41);
		scanf( "%ld", &opc );
      getchar();

      if ( validarRango( opc, y-5 ) == false )
      Entorno_Mensaje_Error();

	}while( validarRango( opc, y-5 ) == false );

	return opc;
}

/**
 * @brief Construye Menú 2.
 *
 * @return Opción validada.
 */
size_t Entorno_Menu_2( void )
{
   int y = 2;
	size_t opc;

	do{
      y = 2;
      Entorno_Construir();
      Entorno_Gotoxy( 2, y );
		printf( "Buscar en la biblioteca" ); y=y+2; Entorno_Gotoxy( 2, y );

		printf( "%d Buscar un artista", y-3 ); y=y+1; Entorno_Gotoxy( 2, y );
		printf( "%d Buscar una canción", y-3 ); y=y+1; Entorno_Gotoxy( 2, y );
		printf( "%d Buscar un álbum", y-3 ); y=y+1; Entorno_Gotoxy( 2, y );
		printf( "%d Buscar un género", y-3 ); y=y+1; Entorno_Gotoxy( 2, y );
		printf( "%d Regresar al menú anterior", y-3 ); y=y+2; Entorno_Gotoxy( 2, y );

      printf( "Por favor, seleccione una opcion" );

      Entorno_Gotoxy(5,41);
		scanf( "%ld", &opc );
      getchar();

      if ( validarRango( opc, y-5 ) == false )
      Entorno_Mensaje_Error();

	}while( validarRango( opc, y-5 ) == false );

	return opc;
}

/**
 * @brief Construye Menú 3.
 *
 * @return Opción validada.
 */
size_t Entorno_Menu_3( void )
{
   int y = 2;
	size_t opc;

	do{
      y = 2;
      Entorno_Construir();
      Entorno_Gotoxy( 2, y );
		printf( "Modificar la biblioteca" ); y=y+2; Entorno_Gotoxy( 2, y );

		printf( "%d Agregar un álbum", y-3 ); y=y+1; Entorno_Gotoxy( 2, y );
      printf( "%d Duplicar un álbum", y-3 ); y=y+1; Entorno_Gotoxy( 2, y );
		printf( "%d Modificar un álbum", y-3 ); y=y+1; Entorno_Gotoxy( 2, y );
      printf( "%d Eliminar un álbum", y-3 ); y=y+1; Entorno_Gotoxy( 2, y );
      printf( "%d Eliminar los álbumes de un artista en común", y-3 ); y=y+1; Entorno_Gotoxy( 2, y );
      printf( "%d Eliminar los álbumes de un género en común", y-3 ); y=y+1; Entorno_Gotoxy( 2, y );
      printf( "%d Eliminar los álbumes de un año en común", y-3 ); y=y+1; Entorno_Gotoxy( 2, y );
      printf( "%d Eliminar los álbumes de una calificación en común", y-3 ); y=y+1; Entorno_Gotoxy( 2, y );
      printf( "%d Eliminar TODOS los álbumes", y-3 ); y=y+1; Entorno_Gotoxy( 2, y );
		printf( "%d Regresar al menú anterior", y-3 ); y=y+2; Entorno_Gotoxy( 2, y );

      printf( "Por favor, seleccione una opcion" );

      Entorno_Gotoxy(5,41);
		scanf( "%ld", &opc );
      getchar();

      if ( validarRango( opc, y-5 ) == false )
      Entorno_Mensaje_Error();

	}while( validarRango( opc, y-5 ) == false );

	return opc;
}

/**
 * @brief Construye Menú 1 Submenú 5 y 6.
 *
 * @param word palabra modificadora.
 *
 * @return Opción validada.
 */
size_t Entorno_Menu_1_5y6( char* word )
{
   int y = 2;
	size_t opc;

	do{
      y = 2;
      Entorno_Construir();
      Entorno_Gotoxy( 2, y );
		printf( "Mostrar por calificación de %s", word ); y=y+2; Entorno_Gotoxy( 2, y );

		printf( "%d Mostrar de calificación ★★★★★ estrellas", y-3 ); y=y+1; Entorno_Gotoxy( 2, y );
      printf( "%d Mostrar de calificación ★★★★☆ estrellas", y-3 ); y=y+1; Entorno_Gotoxy( 2, y );
      printf( "%d Mostrar de calificación ★★★☆☆ estrellas", y-3 ); y=y+1; Entorno_Gotoxy( 2, y );
      printf( "%d Mostrar de calificación ★★☆☆☆ estrellas", y-3 ); y=y+1; Entorno_Gotoxy( 2, y );
      printf( "%d Mostrar de calificación ★☆☆☆☆ estrellas", y-3 ); y=y+1; Entorno_Gotoxy( 2, y );
      printf( "%d Mostrar de calificación ☆☆☆☆☆ estrellas", y-3 ); y=y+1; Entorno_Gotoxy( 2, y );
      printf( "%d Mostrar sin calificación", y-3 ); y=y+1; Entorno_Gotoxy( 2, y );
		printf( "%d Regresar al menú anterior", y-3 ); y=y+2; Entorno_Gotoxy( 2, y );

      printf( "Por favor, seleccione una opcion" );

      Entorno_Gotoxy(5,41);
		scanf( "%ld", &opc );
      getchar();

      if ( validarRango( opc, y-5 ) == false )
      Entorno_Mensaje_Error();

	}while( validarRango( opc, y-5 ) == false );

	return opc;
}

/**
 * @brief Construye Menú 3 submenú 3.
 *
 * @return Opción validada.
 */
size_t Entorno_Menu_3_3( void )
{
   int y = 2;
	size_t opc;

	do{
      y = 2;
      Entorno_Construir();
      Entorno_Gotoxy( 2, y );
		printf( "Modificar un álbum" ); y=y+1; Entorno_Gotoxy( 2, y );
      printf( "Indicaciones: Elija un campo a modificar" ); y=y+2; Entorno_Gotoxy( 2, y );

		printf( "%d Modificar el nombre del artista", y-4 ); y=y+1; Entorno_Gotoxy( 2, y );
      printf( "%d Modificar el nombre del álbum", y-4 ); y=y+1; Entorno_Gotoxy( 2, y );
      printf( "%d Modificar el comentario del álbum", y-4 ); y=y+1; Entorno_Gotoxy( 2, y );
      printf( "%d Modificar el año del álbum", y-4 ); y=y+1; Entorno_Gotoxy( 2, y );
      printf( "%d Modificar el género del álbum", y-4 ); y=y+1; Entorno_Gotoxy( 2, y );
      printf( "%d Modificar el nombre de una canción", y-4 ); y=y+1; Entorno_Gotoxy( 2, y );
      printf( "%d Modificar la calificación de una canción", y-4 ); y=y+1; Entorno_Gotoxy( 2, y );
		printf( "%d Modificar la calificación de un álbum", y-4 ); y=y+2; Entorno_Gotoxy( 2, y );

      printf( "Por favor, seleccione una opcion" );

      Entorno_Gotoxy(5,41);
		scanf( "%ld", &opc );
      getchar();

      if ( validarRango( opc, y-5 ) == false )
      Entorno_Mensaje_Error();

	}while( validarRango( opc, y-5 ) == false );

	return opc;
}

/**
 * @brief Construye Menú 4.
 *
 * @return Opción validada.
 */
size_t Entorno_Menu_4( void )
{
   int y = 2;
	size_t opc;

	do{
      y = 2;
      Entorno_Construir();
      Entorno_Gotoxy( 2, y );
		printf( "Opciones de depuración" ); y=y+2; Entorno_Gotoxy( 2, y );

		printf( "%d Cargar la biblioteca", y-3 ); y=y+1; Entorno_Gotoxy( 2, y );
      printf( "%d Guardar la biblioteca", y-3 ); y=y+1; Entorno_Gotoxy( 2, y );
		printf( "%d Imprimir estructura de la lista (raw)", y-3 ); y=y+1; Entorno_Gotoxy( 2, y );
      printf( "%d Regresar al menú anterior", y-3 ); y=y+2; Entorno_Gotoxy( 2, y );

      printf( "Por favor, seleccione una opcion" );

      Entorno_Gotoxy(5,41);
		scanf( "%ld", &opc );
      getchar();

      if ( validarRango( opc, y-5 ) == false )
      Entorno_Mensaje_Error();

	}while( validarRango( opc, y-5 ) == false );

	return opc;
}